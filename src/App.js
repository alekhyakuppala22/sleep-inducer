import { BrowserRouter, Routes, Route } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import "./App.css";
import MainPage from "./components/MainPage";
import CardDetails from "./components/CardDetails";
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import Menu from "./components/Options";
import LoggedInMain from "./components/LoggedInMainPage";
import { MusicInfoProvider } from "./Contexts/MusicInfoContext";
import { StoryInfoProvider } from "./Contexts/StoryInfoContext";
import StoryCardDetails from "./components/StoryCardDetails";

const theme = createTheme({
  palette: {
    background: {
      default: "black",
    },
    primary: {
      main: "#7FFFD4",
    },
    secondary: {
      main: "#fff",
    },
  },
  typography: {
    // fontFamily: [
    //   '-apple-system',
    //   'BlinkMacSystemFont',
    //   '"Segoe UI"',
    //   'Roboto',
    //   '"Helvetica Neue"',
    //   'Arial',
    //   'sans-serif',
    //   '"Apple Color Emoji"',
    //   '"Segoe UI Emoji"',
    //   '"Segoe UI Symbol"',
    // ].join(','),
    fontFamily: ['Righteous'],

    fontWeightLight: 100,
    fontWeightRegular: 200,
    fontWeightMedium: 300,
    fontWeightBold: 400,
  },
  props: {
    MuiIcon: {
      color: '#aa0011',
      // color: '#7FFFD4',
    }
  },
  components: {
    MuiIcon: {
      styleOverrides: {
        root: {
          primary: "#fff",
          // primary: "#7FFFD4",
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          border: "none",
          outline: "none",
          borderRadius: "100px",
          textTransform: "lowercase",
          "&:focus": {
            outline: "none",
          },
        },
      },
    },
    MuiDialogContent: {
      styleOverrides: {
        root: {
          backgroundColor: "#000",
        },
      },
    },
  },
});

function App() {
  return (
    <ThemeProvider theme={theme}>
      <BrowserRouter basename={process.env.PUBLIC_URL}>
        <MusicInfoProvider>
          <StoryInfoProvider>
            <Routes>
              <Route path="/" exact element={<MainPage />} />
              <Route path="/Login" exact element={<Login />} />
              <Route path="/SignUp" exact element={<SignUp />} />
              <Route path="/Login/Menu" exact element={<Menu />} />
              <Route path="/Home" exact element={<LoggedInMain />} />
              <Route path="/SignUp/Menu" exact element={<Menu />} />
              <Route path="/openCard/:id" exact element={<CardDetails />} />
              <Route path="/openStoryCard/:id" exact element={<StoryCardDetails />} />
            </Routes>
          </StoryInfoProvider>
        </MusicInfoProvider>
      </BrowserRouter>
    </ThemeProvider>
  );
}

export default App;