import { createContext } from "react";

export const StoryInfoContext = createContext();

export const StoryInfoProvider = (props) => {
  const story = [
    {
      id: "1",
      image: "https://i.ytimg.com/vi/-JR8ofaJuAc/hqdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/-JR8ofaJuAc/hqdefault.jpg",
      title: "the fox and the mama bird",
      youtube:
      "https://www.youtube.com/watch?v=-JR8ofaJuAc",
    },
    {
      id: "2",
      image: "https://i.ytimg.com/vi/n8tDU7RTro0/hqdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/n8tDU7RTro0/hqdefault.jpg",
      title: "selfish gaint",
      youtube:
        "https://www.youtube.com/watch?v=n8tDU7RTro0",
    },
    {
      id: "3",
      image: "https://i.ytimg.com/vi/ITdkZEMXQbU/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/ITdkZEMXQbU/maxresdefault.jpg",
      title: "the ant and grasshopper",
      youtube:
        "https://www.youtube.com/watch?v=ITdkZEMXQbU",
    },
    {
      id: "4",
      image: "https://i.ytimg.com/vi/bdbDHOOmdAo/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/bdbDHOOmdAo/maxresdefault.jpg",
      title: "the mouses mirror",
      youtube:
        "https://www.youtube.com/watch?v=bdbDHOOmdAo",
    },
    {
      id: "5",
      image: "https://i.ytimg.com/vi/v_E-aiPJvaU/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/v_E-aiPJvaU/maxresdefault.jpg",
      title: "the three ants and their beautiful car",
      youtube:
        "https://www.youtube.com/watch?v=v_E-aiPJvaU",
    },
    {
      id: "6",
      image: "https://i.ytimg.com/vi/h2Xj-A7HsYE/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/h2Xj-A7HsYE/maxresdefault.jpg",
      title: "the elephant and the ant",
      youtube:
        "https://www.youtube.com/watch?v=h2Xj-A7HsYE",
    },
    {
      id: "7",
      image: "https://i.ytimg.com/vi/ymVWNdGxdHQ/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/ymVWNdGxdHQ/maxresdefault.jpg",
      title: "the stubborn baby elephant",
      youtube:
        "https://www.youtube.com/watch?v=ymVWNdGxdHQ",
    },
    {
      id: "8",
      image: "https://i.ytimg.com/vi/tUjOL_Nk6uo/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/tUjOL_Nk6uo/maxresdefault.jpg",
      title: "The lion, mouse and the sleepy bear ",
      youtube:
        "https://www.youtube.com/watch?v=tUjOL_Nk6uo",
    },
    {
      id: "9",
      image: "https://i.ytimg.com/vi/ouSSzWAacnY/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/ouSSzWAacnY/maxresdefault.jpg",
      title: "fairy tale",
      youtube:
        "https://www.youtube.com/watch?v=ouSSzWAacnY",
    },
    {
      id: "10",
      image: "https://i.ytimg.com/vi/SAPfI9qsOF8/maxresdefault.jpg",
      imageCredit: "https://i.ytimg.com/vi/SAPfI9qsOF8/maxresdefault.jpg",
      title: "the lazy girl",
      youtube:
        "https://www.youtube.com/watch?v=SAPfI9qsOF8",
    },
    
    ];

    return (
        <StoryInfoContext.Provider value={story}>
            {props.children}
        </StoryInfoContext.Provider>
    );
};

StoryDialogue.js



//import React, { useContext } from "react";
// import Grid from "@mui/material/Grid";
// import GridItem from "./GridItem";
// import { MusicInfoContext } from "../Contexts/MusicInfoContext";
//import { AppBar, Toolbar, Typography } from "@mui/material";
// import { Link } from "react-router-dom";


//added
import React, { useEffect, useRef } from "react";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import { Box } from "@mui/system";
import StoryGridOuter from "./StoryGrid";
import { Typography, Button } from "@mui/material";
//working
/*
const GridOuter = () => {
    const music = useContext(MusicInfoContext);

    return (
        <div>
            <div className="App">
                <AppBar
                    position="absolute"
                    style={{ background: "none", boxShadow: "none" }}
                >
                    <Toolbar>
                        <Typography
                            fontWeight="fontWeightMedium"
                            sx={{ fontSize: "calc(10px + 1.5vmin)" }}
                        >
                            <Link to="/" style={{ color: "#D093C3", textDecoration: "none", marginRight: 1110 }}>
                                Home
                            </Link>
                            <Link to="/Login/Menu" style={{ color: "#D093C3", textDecoration: "none" }}>
                                Back
                            </Link>
                        </Typography>
                    </Toolbar>
                </AppBar>
            </div>
            <div className="grid">
                <Grid container alignItems="stretch" spacing={4} >
                    {music.map((music) => (
                        <Grid key={music.id} xs={12} sm={6} lg={3} item>
                            <GridItem music={music} />
                        </Grid>
                    ))}
                </Grid>
            </div>
        </div>
    );
};

export default GridOuter;*/

function StoryDialog({ isStoryOpen, setIsStoryOpen }) {
    const handleStoryClose = () => {
      setIsStoryOpen(false);
    };
  
    const descriptionElementRef = useRef(null);
    useEffect(() => {
      if (isStoryOpen) {
        const { current: descriptionElement } = descriptionElementRef;
        if (descriptionElement !== null) {
          descriptionElement.focus();
        }
      }
    }, [isStoryOpen]);
  
    return (
      <Dialog
        open={isStoryOpen}
        onStoryClose={handleStoryClose}
        scroll="paper"
        width="500px"
        maxWidth="100%"
        sx={{ opacity: "0.92" }}
      >
        <Box
          spacing={3}
          sx={{ backgroundColor: "black", padding: "1vmin", display: "flex" }}
        >
          <Typography
            variant="h6"
            align="center"
            sx={{
              color: "#7FFFD4",
              fontFamily: "poppins",
              fontWeight: "200",
              flex: "1",
            }}
          >
            Select your story
          </Typography>
          <Button
            sx={{ fontSize: "calc(5px + 1.5vmin)", padding: "-3px -25px" }}
            onClick={() => {
              setIsStoryOpen(false);
            }}
          >
            Close
          </Button>
        </Box>
        <DialogContent dividers={true}>
          <DialogContentText ref={descriptionElementRef} tabIndex={-1}>
            <StoryGridOuter setIsStoryOpen={setIsStoryOpen} />
          </DialogContentText>
        </DialogContent>
      </Dialog>
    );
  }
  
  export default StoryDialog;
