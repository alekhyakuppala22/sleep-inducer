import axios from "axios";
import React from 'react';
import { useState, useEffect } from "react";

export default function SignUp () {
    const initialValues = {empName: "", salary: "", email: "", gender: "", password: "", joinDate: ""};
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);

    const handleChange = (e) => {
        const {name, value} = e.target;
        setFormValues({ ...formValues, [name]: value });
        console.log(formValues);
    };

    const handleSubmit = async(e) => {
        e.preventDefault();
        
        setFormErrors(validate(formValues));
        setIsSubmit(true);
        const userObject = {
            empName: formValues.empName,
            salary: formValues.salary,
            email: formValues.email,
            gender: formValues.gender,
            password: formValues.password,
            joinDate: formValues.joinDate,
        };
        axios.post('http://localhost:3000/register', userObject)
            .then((res) => {
                console.log(res.data)
            });
        
    };

    useEffect(() => {
        console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.empName){
            errors.empName = "Name is required !"
        }
        if (!values.salary){
            errors.salary = "Salary is required !"
        }
        if (!values.email){
            errors.email = "Email is required !"
        }   else if (!regex.test(values.email)){
            errors.email = "This is an invalid email !"
        }
        if (!values.gender){
            errors.gender = "Gender is required !"
        }
        if (!values.password){
            errors.password = "Password is required !"
        }   else if (values.password.length < 6){
            errors.password = "Password must be atleast 6 characters !"
        }
        if (!values.joinDate){
            errors.joinDate = "Date of Joining is required !"
        }
        return errors
    }

    return (
        <form onSubmit={handleSubmit}>
            <h3>Register</h3>

            <div className="form-group">
                <label>Full Name</label>
                <input type="text" name = "empName" className="form-control" placeholder="Full name" value={formValues.empName} onChange={handleChange}/>
                <p>{formErrors.empName}</p>
            </div>

            <div className="form-group">
                <label>Salary</label>
                <input type="number" name = "salary" className="form-control" placeholder="Salary" value={formValues.salary} onChange={handleChange} />
                <p>{formErrors.salary}</p>
            </div>

            <div className="form-group">
                <label>Email</label>
                <input type="email" name = "email" className="form-control" placeholder="Enter email" value={formValues.email} onChange={handleChange} />
                <p>{formErrors.email}</p>
            </div>

            <div className="form-group">
                <label>Gender</label>
                <input type="text" name = "gender" className="form-control" placeholder="Gender" value={formValues.gender} onChange={handleChange} />
                <p>{formErrors.email}</p>
            </div>


            <div className="form-group">
                <label>Password</label>
                <input type="password" name = "password" className="form-control" placeholder="Password" value={formValues.password} onChange={handleChange}/>
                <p>{formErrors.password}</p>
            </div>

            <div className="form-group">
                <label>Date of Joining</label>
                <input type="date" name = "joinDate" className="form-control" placeholder="Date of Joining" value={formValues.joinDate} onChange={handleChange}/>
                <p>{formErrors.password}</p>
            </div>

            <button type="submit" className="btn btn-dark btn-lg btn-block">Register</button>
            <p className="forgot-password text-right">
                Already registered <a href="#">log in?</a>
            </p>
        </form>
    );
}