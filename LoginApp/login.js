import { useState, useEffect } from "react";
import React from 'react';

export default function Login() {
    const initialValues = {email: "", password: ""};
    const [formValues, setFormValues] = useState(initialValues);
    const [formErrors, setFormErrors] = useState({});
    const [isSubmit, setIsSubmit] = useState(false);

    const handleChange = (e) => {
        const {name, value} = e.target;
        setFormValues({ ...formValues, [name]: value });
        console.log(formValues);
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setFormErrors(validate(formValues));
        setIsSubmit(true);
    };

    useEffect(() => {
        console.log(formErrors);
        if (Object.keys(formErrors).length === 0 && isSubmit) {
            console.log(formValues);
        }
    }, [formErrors]);

    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email){
            errors.email = "Email is required !"
        }   else if (!regex.test(values.email)){
            errors.email = "This is an invalid email !"
        }
        if (!values.password){
            errors.password = "Password is required !"
        }   else if (values.password.length < 6){
            errors.password = "Password must be atleast 6 characters !"
        }
        return errors
    }

    return (
        <form onSubmit={handleSubmit}>

            <h3>Log in</h3>
            <div className="form-group">
                <label>Email</label>
                <input type="email" name = "email" className="form-control" placeholder="Email ID" value={formValues.email} onChange={handleChange} />
                <p>{formErrors.email}</p>
            </div>

            <div className="form-group">
                <label>Password</label>
                <input type="password" name = "password" className="form-control" placeholder="Password" value={formValues.password} onChange={handleChange}/>
                <p>{formErrors.password}</p>
            </div>

            <div className="form-group">
                <div className="custom-control custom-checkbox">
                    <input type="checkbox" className="custom-control-input" id="customCheck1" />
                    <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                </div>
            </div>

            <button type="submit" className="btn btn-dark btn-lg btn-block">Sign in</button>
            <p className="forgot-password text-right">
                Forgot <a href="#">password?</a>
            </p>
        </form>
    );
}