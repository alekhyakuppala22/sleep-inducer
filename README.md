## SLEEP INDUCER 

This is a part of our WISE 303 project.

The web application offers a comprehensive library of sleep-inducing sounds and noises, as well as additional programs to help you unwind before bedtime. 
Once the user clicks on their choice of music/sounds, the lights will gradually start to dim and the volume will start to go down.
In addition to this, the user can also set the volume and the brightness level of the screen. 
Nature sounds are also available within the web app which include ocean waves, rain, and wind.
The app even has a collection of bed time stories making our app kid-friendly.

## Team Members:

Akanksha:       19WH1A0470  ECE

K.Alekhya:      19WH1A05G7  CSE 

B.Sai Deepthi:  19WH1A0476  ECE

J.Nikhitha(TL):     19WH1A1207  IT

Soma Himaja:    19WH1A04B1  ECE
