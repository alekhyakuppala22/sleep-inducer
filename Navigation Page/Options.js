import React, { useState } from "react";
import DialogComponent from "./DialogComponent";
import Button from "@mui/material/Button";
import { AppBar, Toolbar, Typography } from "@mui/material";
import { Link } from "react-router-dom";

//added


export default function MainPage() {
    const [isOpen, setIsOpen] = useState(false);
    const myStyle = {
        height: "100vh",
        justifyContent: "center",
        whiteSpace: "pre-line",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
        padding: "12vmin",
    };

    const handleClick = () => {
        setIsOpen(true);
    };

    return (
        <div className="App">
            <AppBar
                position="absolute"
                style={{ background: "none", boxShadow: "none" }}
            >
                <Toolbar>
                    <Typography
                        fontWeight="fontWeightMedium"
                        sx={{ fontSize: "calc(10px + 1.5vmin)" }}
                    >
                        <Link to="/" style={{ color: "#D093C3", textDecoration: "none", marginRight: 1110 }}>
                            Home
                        </Link>
                        <Link to="/" style={{ color: "#D093C3", textDecoration: "none" }}>
                            Sign Out
                        </Link>
                    </Typography>
                </Toolbar>
            </AppBar>
            <div style={myStyle}>
                <div class="split left">
                    <div class="centered">
                        <Typography
                            sx={{ fontSize: "calc(5px + 5vmin)" }}
                            color="primary"
                            fontWeight="fontWeightLight"
                        >
                            Sleep Sounds
                        </Typography>
                        <Typography
                            color="secondary"
                            fontWeight="fontWeightMedium"
                            style={{ fontSize: "calc(10px + 1.3vmin)" }}
                        >
                            Sleep Sounds are a collection calming tunes which helps you relax
                            <br /><br />
                        </Typography>
                        <img src={require("../Contexts/sleepsounds.jpg")} height="200" />

                        <div>
                            <Button
                                variant="contained"
                                onClick={handleClick}
                                color="primary"
                                style={{
                                    marginTop: "5vmin",
                                    fontSize: "calc(10px + 1.5vmin)",
                                    padding: "3px 25px",
                                }}
                            >
                                click here
                            </Button>
                        </div>
                        <DialogComponent
                            isOpen={isOpen}
                            setIsOpen={setIsOpen}
                            style={{ borderColor: "red" }}
                        />
                    </div>
                </div>
                <div class="split right">
                    <div class="centered">
                        <Typography
                            sx={{ fontSize: "calc(5px + 5vmin)" }}
                            color="primary"
                            fontWeight="fontWeightLight"
                        >
                            Bedtime Stories
                        </Typography>
                        <Typography
                            color="secondary"
                            fontWeight="fontWeightMedium"
                            style={{ fontSize: "calc(10px + 1.3vmin)" }}
                        >
                            Sleep Sounds are a collection calming tunes which helps you relax
                            <br /><br />
                        </Typography>
                        <img src={require("../Contexts/bedtimestories.jpg")} height="200" width="270" />
                        <div>
                            <Button
                                variant="contained"
                                color="primary"
                                style={{
                                    marginTop: "5vmin",
                                    fontSize: "calc(10px + 1.5vmin)",
                                    padding: "3px 25px",
                                }}
                            >
                                <Link to="./Stories" style={{ color: "black" }}>
                                    click here
                                </Link>
                            </Button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}