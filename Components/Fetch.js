import axios from 'axios'
import React, { Component } from 'react'

export default class Fetch extends Component {
  
  constructor(){
    super()
    this.state = {
      records : []
    }
  }

  getData(){
    axios.get('http://localhost:3000/fetch').then((result)=>{
      console.log(result); this.setState({
        records : result.data
      })
    })
  }

  render() {
    return (
      <div>
        <table class="table table-bordered table table-hover table-sm">
          <thead>
            <tr className="table-head"><th>ID</th><th>Name</th><th>Gender</th><th>Date of Join</th><th>Salary</th><th>Email ID</th></tr>
          </thead>
          <tbody className='labels'>
            {this.state.records.map((element) => (
              <tr><td>{element._id}</td>
              <td>{element.empName}</td>
              <td>{element.gender}</td>
              <td>{element.joinDate}</td>
              <td>{element.salary}</td>
              <td>{element.email}</td>
              </tr>
            ))}
          </tbody>
        </table>
        <button onClick={()=>this.getData()}> Display Data </button>
        
      </div>
    )
  }
}
