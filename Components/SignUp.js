import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { AppBar, Toolbar, Typography } from "@mui/material";
import { blue } from "@mui/material/colors";

class SignUp extends Component {
    state = {
        name: '',
        email: '',
        gender: '',
        password: '',
        records: [],
        mails: [],
        fireErrors: ''
    }

    myStyle = {
        height: "100vh",
        justifyContent: "center",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
    };

    getDetails() {
        axios.get('http://localhost:3000/fetch').then((result) => {
            this.state.records = result.data;
            var flag = 0;
            for (var i = 0; i < this.state.records.length; i++) {
                if ((this.state.records[i].email === this.state.email) == true) {
                    alert("E-mail already exists");
                    flag = 1;
                    return;
                }
            }
            if (flag == 0) {
                
                axios.post('http://localhost:3000/register', this.state).then((result) => {
                    console.log(result);
                    alert("User now registered!\n You can Log in now");
                }).catch(error => {
                    console.log(error)
                })
            }
        })
    }

    changeHandler = (event) => {
        this.setState({ [event.target.name]: event.target.value })
    }

    handleSubmit = (event) => {
        var ans = this.getDetails();

        event.preventDefault();
    }

    render() {
        return (
            <div>
                <div className="App">
                    <AppBar
                        position="absolute"
                        style={{ background: "#446966", boxShadow: "none" }}
                    >
                        <Toolbar>
                            <Typography
                                fontWeight="fontWeightMedium"
                                sx={{ fontSize: "calc(10px + 1.5vmin)" }}
                            >
                                <Link to="/" style={{ color: "aquamarine", textDecoration: "none", marginRight: 1200 }}>
                                    <button type="button" class="btn btn-dark">
                                        Home
                                    </button>
                                </Link>
                            </Typography>
                        </Toolbar>
                    </AppBar>
                </div>
                <form onSubmit={this.handleSubmit}>
                    <div className="bg">
                        <div style={this.myStyle}>
                            <Typography sx={{ fontSize: "calc(2px + 6vmin)" }} color="primary" fontWeight="fontWeightLight" style={{ marginBottom: 30, marginTop: 30 }}>
                                Sign Up
                            </Typography>
                            <div className="form-group">
                                <label className="labels">Full Name</label>
                                <input type="text" name="name" className="form-control" placeholder="Full name" onChange={this.changeHandler} />
                            </div>
                            <div className="form-group">
                                <label className="labels">Email</label>
                                <input type="email" name="email" className="form-control" placeholder="Enter email" onChange={this.changeHandler} />
                            </div>

                            <div className="form-group">
                                <label className="labels">Gender</label>
                                <input type="text" name="gender" className="form-control" placeholder="Gender" onChange={this.changeHandler} />
                            </div>


                            <div className="form-group">
                                <label className="labels">Password</label>
                                <input type="password" name="password" className="form-control" placeholder="Password" onChange={this.changeHandler} />
                            </div>

                            <button type="submit" style={{
                                marginTop: "0vmin",
                                fontSize: "calc(10px + 1.5vmin)",
                                padding: "3px 25px",
                                borderRadius: "100px",
                                backgroundColor: "aquamarine",
                                fontFamily: ['Righteous']
                            }}>
                                Sign Up
                            </button>
                            <p className="forgot-password text-right">
                                Already registered  <Link to="/Login" style={{ color: blue, textDecoration: "none", marginRight: 20 }}>
                                    Login
                                </Link>
                            </p>
                        </div></div>
                </form></div>
        );
    }
}
export default SignUp;
