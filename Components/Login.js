import React, { useContext, useState } from "react"
import axios from "axios"
import { useNavigate } from "react-router-dom"
import Menu from "./Options";
import { Link } from "react-router-dom";
import { AppBar, Toolbar, Typography } from "@mui/material";

const Login = () => {
    const navigate = useNavigate();
    const [user, setUser] = useState({
        email: "",
        password: "",
    })
    const [formErrors, setFormErrors] = useState({});
    const [records, setRecord] = useState([]);
    const handleChange = e => {
        const { name, value } = e.target
        setUser({
            ...user,
            [name]: value
        })
    }
    const handleSubmit = (e) => {
        e.preventDefault();
        login()
    }
    const validate = (values) => {
        const errors = {};
        const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
        if (!values.email) {
            errors.email = "Email is required *"
        } else if (!regex.test(values.email)) {
            errors.email = "This is an invalid email *"
        }
        if (!values.password) {
            errors.password = "Password is required *"
        } else if (values.password.length < 6) {
            errors.password = "Password must be atleast 6 characters *"
        }
        return errors
    }

    const login = () => {
        axios.get("http://localhost:3000/fetch")
            .then((res) => {
                var flag = false;
                for (var i = 0; i < res.data.length; i++) {
                    if ((res.data[i].email === user.email) && (res.data[i].password === user.password)) {
                        flag = 1;
                        break;
                    }
                }
                if (flag === 1) {
                    console.log("match")
                    alert("Logged in successfully! \n Welcome to SLEEP INDUCER!")
                    navigate(`/Login/Menu`, { replace: true });
                }
                else {
                    alert("Invalid details \n Please try again.");
                    return;
                }
            })
    }

    const myStyle = {
        height: "100vh",
        justifyContent: "center",
        display: "flex",
        alignItems: "center",
        flexDirection: "column",
    };


    return (
        <div className="App">
            <AppBar
                position="absolute"
                style={{ background: "#446966", boxShadow: "none" }}
            >
                <Toolbar>
                    <Typography
                        fontWeight="fontWeightMedium"
                        sx={{ fontSize: "calc(10px + 1.5vmin)" }}
                    >
                        <Link to="/" style={{ color: "aquamarine", textDecoration: "none", marginRight: 1140 }}>
                            <button type="button" class="btn btn-dark">
                                Home
                            </button>
                        </Link>
                        <Link to="/SignUp" style={{ color: "aquamarine", textDecoration: "none" }}>
                            <button type="button" class="btn btn-dark">
                                Sign Up
                            </button>
                        </Link>
                    </Typography>
                </Toolbar>
            </AppBar>
            <form onSubmit={handleSubmit}>
                <div className="bg">
                    <div style={myStyle}>
                        <Typography sx={{ fontSize: "calc(2px + 7vmin)" }} color="aquamarine" fontWeight="fontWeightLight" style={{ marginBottom: 30 }}>
                            Login
                        </Typography>
                        <div className="form-group">
                            <label className="labels">Email</label>
                            <input type="email" name="email" className="form-control" placeholder="Email ID" value={user.email} onChange={handleChange} />
                            <p>{formErrors.email}</p>
                        </div>

                        <div className="form-group">
                            <label className="labels">Password</label>
                            <input type="password" name="password" className="form-control" placeholder="Password" value={user.password} onChange={handleChange} />
                            <p>{formErrors.password}</p>
                        </div>

                        <div className="form-group">
                            <div className="custom-control custom-checkbox">
                                <input type="checkbox" className="custom-control-input" id="customCheck1" />
                                <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                            </div>
                        </div>
                        <button type="submit" style={{
                            marginTop: "0vmin",
                            fontSize: "calc(10px + 1.5vmin)",
                            padding: "3px 25px",
                            borderRadius: "100px",
                            backgroundColor: "aquamarine",
                            fontFamily: ['Righteous']
                        }}>
                            Login
                        </button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default Login
