import "./App.css";
import MainPage from "./components/MainPage";
import { MusicInfoProvider } from "./Contexts/MusicInfoContext";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'

import Login from "./components/Login";
import SignUp from "./components/SignUp";
import Fetch from "./components/Fetch";

const theme = createTheme({
  palette: {
    background: {
      default: "black",
    },
    primary: {
      main: "#D093C3",
    },
    secondary: {
      main: "#fff",
    },
  },
  typography: {
    fontFamily: "Poppins",
    fontWeightLight: 100,
    fontWeightRegular: 200,
    fontWeightMedium: 300,
    fontWeightBold: 400,
  },
  props: {
    MuiIcon: {
      color: '#aa0011',
    }
  },
  components: {
    MuiIcon:{
      styleOverrides: {
        root: {
          primary: "#fff",
        }
      }
    },
    MuiButton: {
      styleOverrides: {
        root: {
          border: "none",
          outline: "none",
          borderRadius: "100px",
          textTransform: "lowercase",
          "&:focus": {
            outline: "none",
          },
        },
      },
    },
    MuiDialogContent: {
      styleOverrides: {
        root: {
          backgroundColor: "#000",
        },
      },
    },
  },
});
function App() {
  return (
   
    <ThemeProvider theme={theme}>

      <BrowserRouter basename={process.env.PUBLIC_URL}>
      <MusicInfoProvider>
        <Routes>
            <Route path="/" exact element={<MainPage/>}/>
            <Route path="/Login" exact element={<Login/>}/>
            <Route path="/SignUp" exact element={<SignUp/>}/>
            <Route path="/Fetch" exact element={<Fetch/>}/>
        </Routes>
        </MusicInfoProvider>
      </BrowserRouter>

    </ThemeProvider>
  );
}

export default App;
