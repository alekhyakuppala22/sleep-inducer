import { createContext } from "react";

export const MusicInfoContext = createContext();

export const MusicInfoProvider = (props) => {
  const music = [
    {
      id: "1",
      image: "https://c.tenor.com/kuue1kTkjysAAAAC/rain-sailor.gif",
      imageCredit: "https://tenor.com/view/rain-sailor-moon-gif-20481725",
      title: "rain",
      youtube:
        "https://www.youtube.com/watch?v=q76bMs-NwRk",
    },
    {
      id: "2",
      image: "https://media0.giphy.com/media/SWhsTrEYSrGd4CAhNC/giphy.gif?cid=ecf05e47p52d27fgwnp1fvyy4czk8c3qmldmekdm1mhs1j8k&rid=giphy.gif&ct=g",
      imageCredit: "https://giphy.com/gifs/animatr-anime-aesthetics-anime90s-waves-SWhsTrEYSrGd4CAhNC",
      title: "ocean",
      youtube:
        "https://www.youtube.com/watch?v=HCx_L2QwxX4",
    },
    {
      id: "3",
      image: "https://c.tenor.com/2FIMeyZLsD8AAAAC/anime-1980s.gif",
      imageCredit: "https://tenor.com/view/anime-1980s-80s-city-night-gif-16335334",
      title: "traffic",
      youtube:
        "https://youtu.be/iA0Tgng9v9U",
    },
    ];

    return (
        <MusicInfoContext.Provider value={music}>
            {props.children}
        </MusicInfoContext.Provider>
    );
};